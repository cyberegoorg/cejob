#!/usr/bin/env python
import argparse
import socket
from cejob.ext.call_maker import CallMaker, callmaker_main, callmaker_argparse


class SampleRequestMaker(CallMaker):
    def on_prepare(self, server, port, **kwargs):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((server, port))

    def on_call(self, name, call_args):
        self.s.sendall("%s\n" % str({
            'name:': name,
            'args': call_args
        }))


if __name__ == '__main__':
    #Prepare argparse
    arg_parse = argparse.ArgumentParser('Sample call maker')
    arg_parse.add_argument('-S', dest='server', action='store',
                           help='Server IP', required=True, type=str)
    arg_parse.add_argument('-P', dest='port', action='store',
                           help='Server port', required=True, type=int)
    callmaker_argparse(arg_parse)

    # Run main
    args = arg_parse.parse_args()
    callmaker_main(SampleRequestMaker, args=args, server=args.server, port=args.port)